#!/bin/bash
# Copyright (c) 2019 Timur Sultanaev
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


export KIND_PID_LOCATION="/.kind_pid"
export KCP_LOGS_PID_LOCATION="/.logs_pid"

function cleanup() {
    echo "cleanup"
    if [[ -f "${KCP_LOGS_PID_LOCATION}" ]] ; then 
        echo "killing logs reader"
        kill -s TERM "$(cat ${KCP_LOGS_PID_LOCATION})" || echo "could not kill logs reader"
    fi
    if [[ -f "${KIND_PID_LOCATION}" ]] ; then 
        echo "deleting kind cluster"
        kind delete cluster
    fi
    exit $?
}

function get_logs() {
    docker logs kind-control-plane -f &
    printf "$!" > ${KCP_LOGS_PID_LOCATION}
    echo "${KCP_LOGS_PID_LOCATION}: $(cat ${KCP_LOGS_PID_LOCATION})"
}

function trap_for_cleanup() {
    trap cleanup EXIT SIGINT SIGTERM
}

function wait_for_apiserver() {
    echo "running wait for apiserver"
    cat /wait-for-apiserver.sh | docker exec -i kind-control-plane bash
}

