#!/bin/bash
# Copyright (c) 2019 Timur Sultanaev
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


if [[ "$(hostname)" != "kind-control-plane" ]] ; then 
    echo "MUST be run in kind, exiting"
    exit 1
fi

echo "Starting wait for apiserver"

# Assuming we are running in kind
cp /etc/kubernetes/admin.conf ~/.kube/config

pods=$(kubectl get pods -A)
echo "${pods}"
for i in {1..300}; do 
if ! (  printf "${pods}" | grep apiserver | grep -q Running )  ; then 
    printf '\r%2d/300 ' $i
    echo "no apiserver yet, wait a bit"
    sleep 1
    pods=$(kubectl get pods -A)
    echo "${pods}"
else 
    break
fi
done

if ! ( printf "${pods}" | grep -q apiserver ) ; then
    echo "apiserver have not run for 5 minutes, cancel CI"
    exit 1
fi
